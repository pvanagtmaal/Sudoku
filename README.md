# Sudoku
This project contains solvers for Sudoku's in both C and Java.

##Running the C-version
Running the C-version comes down to compiling and then specifying the width/height and
the file containing the sudoku when running the executable. The size should be either
4, 9 or 16 and the textfile should look like this, without any trailing spaces:  
0 2 0 0 0 0 0 4 0  
3 0 0 0 8 0 0 0 0  
0 0 0 0 0 0 0 1 0  
0 0 0 6 3 0 7 0 0  
0 0 1 0 0 0 0 0 0  
0 0 0 0 0 0 3 0 0  
0 5 0 1 0 4 0 0 0  
7 0 0 0 0 0 8 0 6  
0 0 0 2 0 0 0 0 0  
###OSx Mavericks and above
    gcc solver.c -o solver
    ./solver size sudoku
###Linux
    gcc -std=c99 -lm solver.c -o solver
    ./solver size sudoku
