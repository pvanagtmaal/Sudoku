#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

#include "solver.h"

uint8_t size;
uint8_t sqSize;
uint8_t **grid;

/* Used for counting the recursive calls. For statistics only. */
long count = 0;

int main(int argc, char** argv) {
	FILE *f;
	if (argc < 3) {
		arg_err();
	} else {
		size = atoi(argv[1]);
		sqSize = (int) sqrt(size);
		f = fopen(argv[2], "r");
	}
	
	if (!(size == 9 || size == 4 || size == 16)) {
		printf("Size must be either 4, 9 or 16.");
		exit(EXIT_FAILURE);
	}
	
	if (!f) {
		printf("File not found.\n");
		exit(EXIT_FAILURE);
	}
	
	parse_input(f);
	print_grid();
	count = 0;
	clock_t cur = clock();
	solve();
	printf("ran for %.3fs\n", (float)(clock() - cur) / CLOCKS_PER_SEC);
	printf("%ld recursive calls\n", count);
	print_grid();
	free_grid();
	exit(EXIT_SUCCESS);
}

void parse_input(FILE *f) {
	init_grid();
	char next;
	uint8_t row = 0, col = 0;
	while ((next = fgetc(f)) != EOF) {
		if (col >= size || row >= size) {
			format_err();
		}
		if (next == ' ') {
			col++;
		} else if (next == '\n') {
			if (col < size - 1) {
				format_err();
			}
			col = 0;
			row++;
		} else {
			grid[row][col] = next - '0';
		}
	}
	fclose(f);
}

void format_err() {
	printf("No valid input. Sudoku should be a size x size matrix.\n");
	exit(EXIT_FAILURE);	
}

void mem_err() {
	printf("Out of memory.\n");;
	exit(EXIT_FAILURE);
}

void arg_err() {
	printf("No size/filename specified.\n");
	printf("Correct usage: ./a.out size filename where size is either 4, 9 or 16.\n");
	printf("The file should be like this:\n");
	printf("1 2 3 4 0 0 0 0 0\n");
	printf("4 5 6 7 8 9 0 0 0\n");
	printf("7 8 9 1 2 3 0 0 0\n");
	printf("0 0 0 0 0 6 0 0 0\n");
	printf("0 0 0 0 0 7 0 0 0\n");
	printf("0 0 0 0 5 0 0 0 0\n");
	printf("0 3 0 0 0 0 0 0 7\n");
	printf("2 0 4 0 0 0 8 0 0\n");
	printf("8 0 5 0 0 2 0 0 0\n");
	exit(EXIT_FAILURE);
}

void init_grid(void) {
	grid = malloc(size * sizeof(uint8_t *));
	if (grid) {
		for (uint8_t i = 0; i < size; i++) {
			grid[i] = malloc(size * sizeof(uint8_t));
			if (!grid[i]) {
				mem_err();
			}
		}
	} else {
		mem_err();
	}
}

void free_grid(void) {
	for (uint8_t i = 0; i < size; i++) {
		free(grid[i]);
	}
	free(grid);
}

void print_grid(void) {
	for (uint8_t row = 0; row < size; row++) {
		if (row > 0 && row % sqSize == 0) {
			putchar('\n');
		}
		for (uint8_t col = 0; col < size; col++) {
			if (col > 0 && col % sqSize == 0) {
				putchar(' ');
			}
			printf("%d ", grid[row][col]);
		}
		putchar('\n');
	}
	putchar('\n');
}

uint8_t solve(void) {
	uint8_t row, col;
	count++;
	if (!find_empty(&row, &col)) {
		return 1;
	}
	
	for (uint8_t num = 1; num <= size; num++) {
		if (valid_option(&num, &row, &col)) {
			grid[row][col] = num;
			/* See if filling in this number leads to a solution. */
			if (solve()) {
				return 1;
			}
			/* Apply backtracking because the solve() failed. */ 
			grid[row][col] = 0;
		} 
	}
	return 0;
}

uint8_t find_empty(uint8_t *row, uint8_t *col) {
	for (*row = 0; *row < size; (*row)++) {
		for (*col = 0; *col < size; (*col)++) {
			if (grid[*row][*col] == 0) {
				return 1;
			}
		}
	}
	return 0;
}

uint8_t valid_option(uint8_t *num, uint8_t *row, uint8_t *col) {
	return !(used_in_row(num, row) || used_in_col(num, col) 
		|| used_in_square(num, *row - *row % sqSize, *col - *col % sqSize));
}

uint8_t used_in_square(uint8_t *num, uint8_t row, uint8_t col) {
	for (uint8_t i = row; i < row + sqSize; i++) {
		for (uint8_t j = col; j < col + sqSize; j++) {
			if (grid[i][j] == *num) {
				return 1;
			}
		}
	}
	return 0;
}

uint8_t used_in_col(uint8_t *num, uint8_t *col) {
	for (uint8_t i = 0; i < size; i++) {
		if (grid[i][*col] == *num) {
			return 1;
		}
	}
	return 0;
}

uint8_t used_in_row(uint8_t *num, uint8_t *row) {
	for (uint8_t i = 0; i < size; i++) {
		if (grid[*row][i] == *num) {
			return 1;
		}
	}
	return 0;
}