#ifndef SOLVER
#define SOLVER

/* Allocate memory for the grid. */
void init_grid(void);
/* Deallocate memory for the grid. */
void free_grid(void);

/* Prints the sudoku in a readable way. */
void print_grid(void);

/* Parses the file containing the sudoku to be solved. */
void parse_input(FILE *f);


/* Finds the row and column of the first empty cell.
 * Returns 0 if no empty cell is found, 1 otherwise. */
uint8_t find_empty(uint8_t *row, uint8_t *col);
/* Checks if inserting the selected number is violating the constraints
 * of a valid sudoku. If so the function returns 0, 1 otherwise. */
uint8_t valid_option(uint8_t *num, uint8_t *row, uint8_t *col);


/* Checks if a value has been used before in the row it is in.
 * Returns 1 if it has been used before, 0 otherwise. */
uint8_t used_in_row(uint8_t *num, uint8_t *row);
/* Checks if a value has been used before in the column it is in.
 * Returns 1 if it has been used before, 0 otherwise. */
uint8_t used_in_col(uint8_t *num, uint8_t *col);
/* Checks if a value has been used before in the square it is in.
 * Returns 1 if it has been used before, 0 otherwise. */
uint8_t used_in_square(uint8_t *num, uint8_t row, uint8_t col);	

/* Tries to solve the sudoku by backtracking. Numbers 1 to 9 are tried
 * and if inserting the number into the cell results into a valid sudoku,
 * 1 is returned. If an invalid sudoku is created, the value is set back
 * to 0 and the next value in line is tried. This method always results in
 * a valid solution unless an invalid input sudoku is given. */
uint8_t solve(void);

/* Error message printing. */
void format_err();
void mem_err();
void arg_err();

#endif