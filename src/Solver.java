import java.util.Scanner;

public class Solver {

	private int[][] grid;
	private int size;
	private int sqSize;
	private static int count = 0;

	public Solver(int[][] grid) {
		this.grid = grid;
	}
	
	public Solver(int size, String numbers) {
		this.size = size;
		this.sqSize = (int) Math.sqrt(size);
		this.grid = new int[size][size];
		parseNumbers(numbers);
	}

	private void parseNumbers(String numbers) {
		Scanner sc = new Scanner(numbers);
		sc.useDelimiter(" |\n");
		int i = 0;
		while (sc.hasNextLine()) {
			for (int j = 0; j < size; j++) {
				grid[i][j] = sc.nextInt();
			}
			i++;
		}
		sc.close();
	}

	public boolean solve() {
		count++;
		int idx = findEmpty();
		if (idx == -1) {
			return true;
		}
		int row = idx / size;
		int col = idx % size;

		for (int num = 1; num <= 9; num++) {
			if (isValidOption(num, row, col)) {
				grid[row][col] = num;
				if (solve()) {
					return true;
				}
				grid[row][col] = 0;
			}
		}
		return false;
	}

	private int findEmpty() {
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				if (grid[row][col] == 0) {
					return size * row + col;
				}
			}
		}
		return -1;
	}

	private boolean isValidOption(int val, int row, int col) {
		return !isUsedInRow(val, row) && !isUsedInCol(val, col)
				&& !isUsedInSquare(val, row - row % 3, col - col % 3);
	}

	private boolean isUsedInRow(int val, int row) {
		for (int i = 0; i < size; i++) {
			if (grid[row][i] == val) {
				return true;
			}
		}
		return false;
	}

	private boolean isUsedInCol(int val, int col) {
		for (int i = 0; i < size; i++) {
			if (grid[i][col] == val) {
				return true;
			}
		}
		return false;
	}

	private boolean isUsedInSquare(int val, int row, int col) {
		for (int i = row; i < row + sqSize; i++) {
			for (int j = col; j < col + sqSize; j++) {
				if (grid[i][j] == val) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			if (i % sqSize == 0) {
				for (int j = 1; j < size; j++) {
					sb.append("---");
				}
				sb.append("\n");
			}

			for (int j = 0; j < size; j++) {
				if (j % sqSize == 0) {
					sb.append("| ");
				}
				sb.append(grid[i][j] + " ");
			}
			sb.append("|\n");
		}
		for (int j = 1; j < size; j++) {
			sb.append("---");
		}
		sb.append("\n");
		return sb.toString();
	}

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		sb.append("0 2 0 0 0 0 0 4 0\n");
		sb.append("3 0 0 0 8 0 0 0 0\n");
		sb.append("0 0 0 0 0 0 0 1 0\n");
		sb.append("0 0 0 6 3 0 7 0 0\n");
		sb.append("0 0 1 0 0 0 0 0 0\n");
		sb.append("0 0 0 0 0 0 3 0 0\n");
		sb.append("0 5 0 1 0 4 0 0 0\n");
		sb.append("7 0 0 0 0 0 8 0 6\n");
		sb.append("0 0 0 2 0 0 0 0 0");

		Solver s = new Solver(9, sb.toString());
		
		long time = System.currentTimeMillis();
		s.solve();
		System.out.println("run time: " + (System.currentTimeMillis() - time));
		
		System.out.println(s.toString());
		System.out.println(count + " recursive calls");
	}
}
