package bitwise;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solver {

	private static final int SIZE = 9;
	private static final int SQ_SIZE = 3;

	private static int[][] sudoku;

	private static int[] rows;
	private static int[] cols;
	private static int[] squares;

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		sb.append("0 2 0 0 0 0 0 4 0\n");
		sb.append("3 0 0 0 8 0 0 0 0\n");
		sb.append("0 0 0 0 0 0 0 1 0\n");
		sb.append("0 0 0 6 3 0 7 0 0\n");
		sb.append("0 0 1 0 0 0 0 0 0\n");
		sb.append("0 0 0 0 0 0 3 0 0\n");
		sb.append("0 5 0 1 0 4 0 0 0\n");
		sb.append("7 0 0 0 0 0 8 0 6\n");
		sb.append("0 0 0 2 0 0 0 0 0");
		parseInput(sb.toString());

		long time = System.nanoTime();
		solve();
		System.out.println("Ran for: " + (System.nanoTime() - time) / 1000000f
				+ "ms");
		print();

	}

	/* Parse the sudoku String, removing all spaces while doing so. */
	private static void parseInput(String s) {
		sudoku = new int[SIZE][SIZE];
		rows = new int[SIZE];
		cols = new int[SIZE];
		squares = new int[SIZE];

		int rowCount = 0;
		int colCount = 0;
		int sqCount = 0;

		s = s.replaceAll(" ", "");
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new ByteArrayInputStream(s.getBytes())));
		try {
			String line = null;
			while ((line = br.readLine()) != null) {
				colCount = 0;
				for (char c : line.toCharArray()) {
					byte val = (byte) Character.getNumericValue(c);
					sqCount = (rowCount / SQ_SIZE) * SQ_SIZE + colCount
							/ SQ_SIZE;
					sudoku[rowCount][colCount] = val;
					rows[rowCount] |= (1 << (val - 1));
					cols[colCount] |= (1 << (val - 1));
					squares[sqCount] |= (1 << (val - 1));
					colCount++;
				}
				rowCount++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* Solve the sudoku using backtracking. */
	public static boolean solve() {
		final int idx = getEmpty();

		/* If getEmpty() returns -1, the sudoku is solved. */
		if (idx == -1) {
			return true;
		}

		final int row = idx / SIZE;
		final int col = idx % SIZE;

		/*
		 * Try every number for each vacant spot in the sudoku. Fall back to the
		 * default (0) if it fails to complete the sudoku using that value.
		 */
		for (byte num = 1; num <= SIZE; num++) {
			if (isUsedInSquare((row / SQ_SIZE) * SQ_SIZE + col / SQ_SIZE, num)
					|| isUsedInRow(row, num) || isUsedInCol(col, num)) {
				continue;
			}
			insertAt(row, col, num);
			if (solve()) {
				return true;
			}
			remove(row, col, num);
		}
		return false;
	}

	/*
	 * Check if the value is used in square number sq, i.e. check if bit number
	 * val - 1 is set in square number sq.
	 */
	public static boolean isUsedInSquare(int sq, byte val) {
		return (squares[sq] & (1 << (val - 1))) != 0;
	}

	/*
	 * Check if the value is used in row number row, i.e. check if bit number
	 * val - 1 is set in row number row.
	 */
	public static boolean isUsedInRow(int row, byte val) {
		return (rows[row] & (1 << (val - 1))) != 0;
	}

	/*
	 * Check if the value is used in column number col, i.e. check if bit number
	 * val - 1 is set in column number col.
	 */
	public static boolean isUsedInCol(int col, byte val) {
		return (cols[col] & (1 << (val - 1))) != 0;
	}

	/* Insert value val at (row,col) in the grid and update the trackers. */
	public static void insertAt(int row, int col, byte val) {
		sudoku[row][col] = val;

		rows[row] |= (1 << (val - 1));
		cols[col] |= (1 << (val - 1));
		squares[(row / SQ_SIZE) * SQ_SIZE + col / SQ_SIZE] |= (1 << (val - 1));
	}

	/*
	 * Remove value val from (row,col) in the grid by inserting the default
	 * value and update the trackers.
	 */
	public static void remove(int row, int col, byte val) {
		sudoku[row][col] = 0;

		rows[row] &= ~(1 << (val - 1));
		cols[col] &= ~(1 << (val - 1));
		squares[(row / SQ_SIZE) * SQ_SIZE + col / SQ_SIZE] &= ~(1 << (val - 1));
	}

	/* Find the closest vacant spot. Return -1 if the sudoku is full. */
	public static int getEmpty() {
		for (byte row = 0; row < SIZE; row++) {
			for (byte col = 0; col < SIZE; col++) {
				if (sudoku[row][col] == 0) {
					return SIZE * row + col;
				}
			}
		}
		return -1;
	}

	/* Prints the sudoku in a readable way. */
	public static void print() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < SIZE; i++) {
			if (i % SQ_SIZE == 0) {
				for (int j = 1; j < SIZE; j++) {
					sb.append("---");
				}
				sb.append("\n");
			}

			for (int j = 0; j < SIZE; j++) {
				if (j % SQ_SIZE == 0) {
					sb.append("| ");
				}
				sb.append(sudoku[i][j] + " ");
			}
			sb.append("|\n");
		}
		for (int j = 1; j < SIZE; j++) {
			sb.append("---");
		}
		sb.append("\n");
		System.out.println(sb.toString());
	}
}
